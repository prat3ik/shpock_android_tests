import unittest
import datetime
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '8.0'
desired_caps['deviceName'] = 'c4e3f3cd'
desired_caps['fullReset'] = False
desired_caps['noReset'] = False
desired_caps['app'] = '/Users/sanjay/IdeaProjects/value_ad_android/src/test/resources/VAD_test122.apk'
driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)


wait = WebDriverWait(driver, 60, poll_frequency=1)
wait.until(EC.visibility_of_element_located((By.ID, 'com.valuead.lms:id/camp_Name')))

driver.find_element(By.ID, "com.valuead.lms:id/camp_Name").send_keys('dev')
driver.find_element(By.ID, "com.valuead.lms:id/btnSetCmpny").click()


wait.until(EC.visibility_of_element_located((By.ID, "userName")))
driver.find_element(By.ID, "userName").send_keys('app_dev')
driver.find_element(By.ID, "password").send_keys('app_dev_PW!')
driver.hide_keyboard()

driver.find_element(By.ID, "com.valuead.lms:id/btnSignIn").click()

wait.until(EC.visibility_of_element_located((By.XPATH, "//android.widget.ImageView[@resource-id='com.valuead.lms:id/compnay_logo']")))

driver.find_element(By.XPATH, "//android.widget.ImageView[@resource-id='com.valuead.lms:id/compnay_logo']").text

