package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pageobjects.*;
import utils.AppiumUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestCases extends BaseTest {

    LoginPO loginPO;
    SoftAssert softAssert = new SoftAssert();

    @BeforeTest
    @Override
    public void setUpPage() {
        loginPO = new LoginPO(driver);
    }

    @Test
    public void verifyUserCanMakeTheOffer() throws InterruptedException {
        final String username = "";
        final String password = "";

        final String productToBeSearched = "iphone";
        final String offerText = "10000";
        final String messageText = "Hello, I am Pratik and this is my offer";

        loginPO.waitTillPageIsVisible();
        loginPO.tapOnSignUpButton();
        DashboardPO dashboardPO = loginPO.login(username, password);

        dashboardPO.waitTillDashboardPOAppeared();
        dashboardPO.tapOnHintGotItButton();
        dashboardPO.searchItem(productToBeSearched);
        ProductDetailsPO productDetailsPO = dashboardPO.tapOnFirstProduct();
        productDetailsPO.tapOnPrivateOfferButton();
        productDetailsPO.makeAnOffer(offerText, messageText);

        Assert.assertTrue(productDetailsPO.getMakeOfferButton().isDisplayed(), "Make Offer button didn't display");
    }


}
