package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import utils.AppiumUtils;
import utils.PropertyUtils;

public class LoginPO extends BasePO {
    public final static String EXECUTION_TYPE = PropertyUtils.getProperty("execution.type", "aws");

    public LoginPO(AppiumDriver driver) {
        super(driver);
    }

    public void waitTillPageIsVisible() {
        waitUtils.waitForElementToBeVisible(skipButton, driver);
    }

    @AndroidFindBy(id = "com.shpock.android:id/tut_register")
    AndroidElement signUpButton;

    public void tapOnSignUpButton() {
        signUpButton.click();
    }

    @AndroidFindBy(id = "com.shpock.android:id/tut_close")
    AndroidElement skipButton;

    public DashboardPO tapOnSkipButton() {
        skipButton.click();
        acceptAlert();
        return new DashboardPO(driver);
    }

    @AndroidFindBy(id = "com.shpock.android:id/myshpock_tab_profile_email_button")
    AndroidElement loginWithEmailButton;

    public void tapOnLoginWithEmailButton() {
        loginWithEmailButton.click();
    }

    @AndroidFindBy(id = "com.shpock.android:id/login_email_email_text")
    AndroidElement emailField;

    public void typeEmailAddress(String email) {
        waitUtils.staticWait(500);
        waitUtils.waitForElementToBeVisible(emailField, driver);
        emailField.click();
        waitUtils.staticWait(1500);
        driver.findElement(By.id("com.google.android.gms:id/cancel")).click();
        emailField.sendKeys(email);
    }

    @AndroidFindBy(id = "com.shpock.android:id/login_email_password_text")
    AndroidElement passwordField;

    public void typePassword(String password) {
        passwordField.sendKeys(password);
    }

    @AndroidFindBy(id = "com.shpock.android:id/login_email_login_button")
    AndroidElement loginButton;

    public void tapOnLogInButton() {
        loginButton.click();
    }


    public DashboardPO login(String email, String password) {
        tapOnLoginWithEmailButton();
        typeEmailAddress(email);
        typePassword(password1);
        tapOnLogInButton();
        waitUtils.staticWait(2000);
        acceptAlert();
        return new DashboardPO(driver);
    }

    public void acceptAlert() {
        waitUtils.staticWait(1000);
        System.out.println("AppiumUtils.isAlertPresent:    " + AppiumUtils.isAlertPresent(driver));
        if (AppiumUtils.isAlertPresent(driver))
            driver.switchTo().alert().accept();
    }

}
