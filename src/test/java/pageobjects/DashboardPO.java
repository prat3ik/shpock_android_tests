package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class DashboardPO extends BasePO {

    protected DashboardPO(AppiumDriver driver) {
        super(driver);
    }


    @AndroidFindBy(id = "com.shpock.android:id/gotit")
    AndroidElement hintGotItButton;

    public void tapOnHintGotItButton() {
        if (hintGotItButton.isDisplayed())
            hintGotItButton.click();
    }

    public void waitTillDashboardPOAppeared() throws InterruptedException {
        Thread.sleep(500);
        waitUtils.waitForElementToBeVisible(hintGotItButton, driver);
    }


    @AndroidFindBy(id = "com.shpock.android:id/action_search")
    AndroidElement searchButton;

    @AndroidFindBy(id = "com.shpock.android:id/search_src_text")
    AndroidElement searchTextField;

    public void searchItem(String productText) {
        searchButton.click();
        searchTextField.sendKeys(productText);
        waitUtils.staticWait(1500);
        driver.findElement(By.id("com.shpock.android:id/search_item_title")).click();
        tapOnHintGotItButton();
    }

    @AndroidFindBy(id = "com.shpock.android:id/itemContainer")
    AndroidElement firstProductCard;

    public ProductDetailsPO tapOnFirstProduct(){
        firstProductCard.click();
        ProductDetailsPO productDetailsPO = new ProductDetailsPO(driver);
        productDetailsPO.tapOnHintGotItButton();
        productDetailsPO.tapOnHintGotItButton();
        return productDetailsPO;
    }

}
